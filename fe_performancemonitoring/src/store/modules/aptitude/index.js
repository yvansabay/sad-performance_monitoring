import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const aptitude = 'aptitude'

export default ({
  namespaced: true,
  state: {
    aptrecord: [],
  },
  actions: {
    async getAptitudeRecord({commit}, data){
      const res = await AXIOS.get(`${aptitude}?section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_RECORD', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async updateAptitudeRecord({commit}, data){
      const res = await AXIOS.put(`${aptitude}/${data.id}`, data).then(response => {
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async storeAptRecord({commit}, data){
      const res = await AXIOS.post(`${aptitude}?student_id=${data.student_id}&section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    }
  },
  getters: {
   getComponent(state){
     return state.data
   },
  },
  mutations: {
    SET_RECORD(state, data){
      state.aptrecord = data
    },
  },
})