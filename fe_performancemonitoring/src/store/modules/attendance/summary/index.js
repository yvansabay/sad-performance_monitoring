import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../../base/config'

Vue.use(Vuex);

const summary = 'summary'

export default ({
  namespaced: true,
  state: {
    summary: [],
  },
  actions: {
    async getAttendanceSummary({commit}, data){
      const res = await AXIOS.get(`${summary}?section_id=${data.section_id}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_SUMMARY', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
   
  },
  getters: {
    getSummary(state){
      return state.summary;
    },
  },
  mutations: {
    SET_SUMMARY(state, data){
      let meetings = 0;
      for(let i = 0; i < data.length; i++){
        data[i].presentdays = 0
        if(data[i]['attendance'].length > meetings){
          meetings = data[i]['attendance'].length;
        }
        for(let j = 0; j < data[i]['attendance'].length; j++){
          if(data[i]['attendance'][j]['status'] == 'Present'){
            data[i].presentdays++;
          }
        }
      }

      //SET TOTAL MEETINGS
      for(let m = 0; m < data.length; m++){
        data[m].totalmeetings = meetings;
      }
      state.summary = data
    },
  },
})