import axios from 'axios';

const API = axios.create({
  baseURL: 'http://127.0.0.1:8000/api'
});

const bearer_token = localStorage.getItem('auth') || ''
API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`

export default API;