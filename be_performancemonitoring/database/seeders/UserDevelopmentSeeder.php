<?php

namespace Database\Seeders;

use App\Models\Instructor;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserDevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $instructor = Instructor::create([
            'first_name' => 'Yvan',
            'middle_name' => 'Caindoy',
            'last_name' => 'Sabay',
            'gender' => 'Male',
            'component_id' => 1
            ]);

        User::create([
            'instructor_id' => $instructor->id,
            'email' => 'sabayyvan2018@gmail.com',
            'password' => Hash::make('123123')
        ]);
    }
}
