<?php

namespace Database\Seeders;

use App\Models\Component;
use Illuminate\Database\Seeder;

class ComponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $component = [
            ['component' => 'Reserve Officer Training Corps (ROTC) '],
            ['component' => 'Literacy Training Service (LTS)'],
            ['component' => 'Civic Welfare Training Service (CWTS)'],
        ];

        foreach ($component as $c) {
            Component::create($c);
        }
    }
}
