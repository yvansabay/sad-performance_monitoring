<?php

namespace App\Http\Controllers;

use App\Models\Component;
use Illuminate\Http\Request;

class ComponentController extends Controller
{
    // Retrieves all components stored in the database
    public function index(){
        return response()->json(Component::all());
    }
}
