<?php

namespace App\Http\Controllers;

use App\Models\ExaminationType;
use Illuminate\Http\Request;

class ExaminationTypeController extends Controller
{
    public function index(){
        return response()->json(ExaminationType::get());
    }
}
