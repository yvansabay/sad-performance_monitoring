<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function store(Request $request){
        $section = Section::create([
            'section' => $request->section,
            'instructor_id' => Auth::id(),
            'semester_id' => $request->semester_id,
            'school_year_id' => $request->school_year_id
        ]);

        return response()->json(['section' => $section, 'msg' => 'Section saved successfully']);
    }

    public function index(){
        // return response()->json(['msg' => 'hi', 'id' => auth()->user()]);
        return response()->json(Section::where('instructor_id', Auth::id())->where('semester_id', Request()->semester_id)->where('school_year_id', Request()->school_year_id)->get());
    }

    public function destroy($id){
        Section::destroy($id);
        return response()->json(['msg' => 'Section deleted successfully!']);
    }

    public function update(Request $request, $id){
        try {

            $section = Section::where('id', $id)->firstOrFail();
            $section->update($request->all());

            $updated = Section::where('id', $id)->firstOrFail();
            return response()->json($updated);

        } catch(ModelNotFoundException $exception) {
            return response()->json(['message' => 'Section not found']);
        }
    }
}
