<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merit extends Model
{
    use HasFactory;

    protected $fillable = [
        'merit',
        'student_id',
        'school_year_id',
        'semester_id'
    ];
}
