<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceSummary extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'present_days',
        'semester_id',
        'section_id',
        'school_year_id'
    ];
}
