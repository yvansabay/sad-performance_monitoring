<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'first_name',
        'middle_name', 
        'last_name',
        'gender',
        'component_id',
        'section_id',
        'instructor_id',
        'semester_id',
        'school_year_id',
    ];

    public function component(){
        return $this->belongsTo(Component::class, 'component_id', 'id');
    }

    public function section(){
        return $this->belongsTo(Section::class, 'section_id', 'id');
    }

    public function instructor(){
        return $this->belongsTo(Instructor::class, 'instructor_id', 'id');
    }

    public function examination(){
        return $this->hasMany(Examination::class, 'student_id', 'id');
    }

    public function merit(){
        return $this->hasOne(Merit::class, 'student_id', 'id');
    }

    public function attendance(){
        return $this->hasMany(Attendance::class, 'student_id', 'id');
    }
}
